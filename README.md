# Zombie-Run!!

## 소개
항상 좀비라고 핍박을 받던 좀비가 화났다!!!!!
이제 좀비가 되어 사람들을 좀비로 만들자...!!

## 플레이 영상
[![Video Label](https://img.youtube.com/vi/1fG-GrtirJk/sddefault.jpg)](https://www.youtube.com/watch?v=1fG-GrtirJk)

## 게임 방법
1. WASD를 사용해서 좀비 무리를 조종
2. 마우스를 적에게 가져다 놓은다음 좌클릭을 하면 좀비들이 가서 공격을 합니다.
3. 마우스 커서에 적의 체력이 보입니다.
4. 적을 죽였을 경우 그 자리에 새로운 좀비가 나타납니다.
5. 좀비는 각종 물건들을 들 수 있으며, F 키를 눌러 물건을 던져 공격할 수 있습니다.

## 사용한 라이브러리
[DoTween](http://dotween.demigiant.com/) : 움직임을 간단하게 구현 할 때 사용

## 직접 제작한 유틸리티
[UnityManagers](https://github.com/aszd0708/UnityGameManagers) : 저장, 풀링, 팝업등 사용

## 제작
### 기간
2019년 9월 30일 ~ 2019년 10월 9일 
약 10일

### 인원
1명

## 특이사항
2019 전문 인력 양성 프로젝트 콘텐츠 제작 출품작