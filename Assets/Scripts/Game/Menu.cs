﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Menu : MonoBehaviour
{
    public bool isPause = false;
    public GameObject menu;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPause = !isPause;
            MenuAction();
        }

        if(isPause)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void MenuAction()
    {
        if(isPause)
        {
            menu.transform.localScale = new Vector3(1, 1, 0);
        }

        else
        {
            menu.transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        }
    }
}
