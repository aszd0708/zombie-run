﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CineCamFollow : MonoBehaviour
{
    public Transform playerCam;
    // Update is called once per frame
    void Update()
    {
        transform.position = playerCam.position;
        transform.rotation = playerCam.rotation;
    }
}
