﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TankEvent : MonoBehaviour
{
    public Transform[] targetWall = new Transform[2];
    public Transform head;
    public Transform movePos;

    private TankAI AI;
    private TankAttack attack;
    private TankHealth health;
    private TankMovement movement;

    void Awake()
    {
        AI = GetComponent<TankAI>();
        attack = GetComponent<TankAttack>();
        movement = GetComponent<TankMovement>();
        health = GetComponent<TankHealth>();
    }

    private void EventStarter()
    {
        StartCoroutine(Event());
    }

    private IEnumerator Event()
    {
        head.DOLookAt(targetWall[0].position, 1.5f);
        yield return new WaitForSeconds(1.5f);
        attack.SendMessage("EventAttackStarter", targetWall[0].position, SendMessageOptions.DontRequireReceiver);
        yield return new WaitForSeconds(2.0f);
        // 여기서 카메라 이동 (시간 조절하면서 넣기)
        // 이부분에 시간 조절해서 넣기
        yield return new WaitForSeconds(3.0f);
        head.DOLookAt(targetWall[1].position, 1.5f);
        yield return new WaitForSeconds(1.5f);
        attack.SendMessage("EventAttackStarter", targetWall[1].position, SendMessageOptions.DontRequireReceiver);
        yield return new WaitForSeconds(2.0f);
        head.DOLocalRotate(new Vector3(0, 0, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOMove(movePos.position, 5.0f);
        yield return new WaitForSeconds(5.0f);
        yield return new WaitForSeconds(2.0f);
        // 대충 게임 실행하는 함수
        AI.enabled = true;
        attack.enabled = true;
        movement.enabled = true;
        health.enabled = true;
    }
}
