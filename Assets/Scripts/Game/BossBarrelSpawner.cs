﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBarrelSpawner : MonoBehaviour
{
    public Transform randomPos1, randomPos2;
    public float randomTimeStart, randomTimeEnd;

    private PoolManager PM;

    private void Awake()
    {
        PM = FindObjectOfType<PoolManager>();
    }

    private void SpawnerStarter()
    {
        StartCoroutine(Spawner());
    }

    private IEnumerator Spawner()
    {
        while(true)
        {
            GameObject barrel = PM.GetPooledObject(ObejctKind.EXPLOSION, new Vector3(Random.Range(randomPos1.position.x, randomPos2.position.x), 3 , Random.Range(randomPos1.position.z, randomPos2.position.z)));
            yield return new WaitForSeconds(Random.Range(randomTimeStart, randomTimeEnd));
        }
    }
}
