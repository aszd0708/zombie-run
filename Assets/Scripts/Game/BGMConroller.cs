﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMConroller : MonoBehaviour
{
    public AudioSource bgm;

    public void VolumeUp()
    {
        bgm.volume += 0.02f;
        if (bgm.volume >= 0.2f)
            bgm.volume = 0.2f;
    }
}
