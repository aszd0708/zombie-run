﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    public Image fade;

    public void StageStart()
    {
        Time.timeScale = 1;
        StartCoroutine(StageScene());
    }

    public void ExitStart()
    {
        Time.timeScale = 1;
        StartCoroutine(Exit());
    }

    private IEnumerator StageScene()
    {
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Stage0");
    }

    public IEnumerator Exit()
    {
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        Application.Quit();
    }

    public void MainStart()
    {
        Time.timeScale = 1;
        StartCoroutine(MainView());
    }

    private IEnumerator MainView()
    {
        Time.timeScale = 1;
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Main");
    }
}
