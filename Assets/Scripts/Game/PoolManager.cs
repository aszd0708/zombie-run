﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ObejctKind
{
    ZOMBIE, CITIZEN_M, CITIZEN_W, POLICE_BAT, POLICE_PISTOL, PICKABLE, EXPLOSION, MISSILE
}

//오브젝트 풀에 넣어줄 오브젝트의 클래스
[System.Serializable]
public class PooledObject
{
    public GameObject pooledPrefab;     //풀에 넣을 게임오브젝트 레퍼런스
    public int maxPool;                 //오브젝트 풀의 크기
    public ObejctKind KIND;
    [HideInInspector] public List<GameObject> pool = new List<GameObject>();  //게임오브젝트를 저장할 리스트
}

public class PoolManager : MonoBehaviour
{ 
    public List<PooledObject> pooledObjects;    //오브젝트 풀에 넣을 PooledObject들

    // Start is called before the first frame update
    void Start()
    {
        CreatePool();       //오브젝트 풀을 생성해라
    }

    //오브젝트 풀에 게임오브젝트를 생성하는 함수
    private void CreatePool()
    {
        //pooledObjects의 리스트를 순환하며
        foreach (PooledObject PO in pooledObjects)
        {
            //풀링할 오브젝트 종류별로 빈 게임오브젝트를 만들어서
            GameObject objectPool = new GameObject(GetParentName(PO));

            //PoolManager의 자식으로 넣어주고
            objectPool.transform.SetParent(this.transform);

            //풀링할 오브젝트를 생성하여 리스트에 채워넣음
            for (int i = 0; i < PO.maxPool; i++)
            {
                GameObject obj = Instantiate(PO.pooledPrefab);

                //풀링한 오브젝트를 PoolManager 자식으로 넣어줌
                obj.transform.SetParent(objectPool.transform);

                //프리팝의 로테이션 대로 회전해줌
                obj.transform.rotation = PO.pooledPrefab.transform.rotation;

                //이름 변경
                obj.name = PO.pooledPrefab.name + "_" + i.ToString("00");  //01, 02, 03, 04...

                //실제 리스트 풀에 넣어줌
                PO.pool.Add(obj);

                //사용하기 전까지는 비활성화
                obj.SetActive(false);
            }
        }
    }

    //사용한 가능한 풀 오브젝트를 반환하는 함수
    public GameObject GetPooledObject(ObejctKind KIND)
    {
        //pooledObjects 리스트를 순환하면서
        foreach (PooledObject PO in pooledObjects)
        {
            //찾으려는 프리팝과 같은 오브젝트이면
            if (PO.KIND == KIND)
            {
                //사용할 수 있는 풀 오브젝트를 찾아서 반환
                for (int i = 0; i < PO.pool.Count; i++)
                {
                    //비활성화 여부로 사용 가능한 게임오브젝트인지 판단
                    if (PO.pool[i].activeSelf == false)
                    {
                        PO.pool[i].SetActive(true);        //켜 주고
                        return PO.pool[i];         //그 게임오브젝트를 리턴
                    }
                }
            }
        }

        //모두 사용 중이면 null값 리턴
        return null;
    }

    public GameObject GetPooledObject(ObejctKind KIND, Vector3 pos)
    {
        //pooledObjects 리스트를 순환하면서
        foreach (PooledObject PO in pooledObjects)
        {
            //찾으려는 프리팝과 같은 오브젝트이면
            if (PO.KIND == KIND)
            {
                //사용할 수 있는 풀 오브젝트를 찾아서 반환
                for (int i = 0; i < PO.pool.Count; i++)
                {
                    //비활성화 여부로 사용 가능한 게임오브젝트인지 판단
                    if (PO.pool[i].activeSelf == false)
                    {
                        PO.pool[i].SetActive(true);        //켜 주고
                        PO.pool[i].transform.position = pos;
                        return PO.pool[i];         //그 게임오브젝트를 리턴
                    }
                }
            }
        }

        //모두 사용 중이면 null값 리턴
        return null;
    }

    // 순회하면서 kind에 따라서 부모 바꿔줌
    public void SetPooledObject(GameObject obj, ObejctKind KIND)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).name == GetParentName(KIND))
            {
                // 그 부모에 진입했음
                obj.transform.SetParent(transform.GetChild(i));
                // 상속
                obj.SetActive(false);
            }
        }
    }

    private string GetParentName(PooledObject PO)
    {
        switch(PO.KIND)
        {
            case ObejctKind.CITIZEN_M:
                return "Citizen_M";

            case ObejctKind.CITIZEN_W:
                return "Citizen_W";

            case ObejctKind.POLICE_BAT:
                return "Police_Bat";

            case ObejctKind.POLICE_PISTOL:
                return "Police_Pistol";

            case ObejctKind.ZOMBIE:
                return "Zombie";

            case ObejctKind.PICKABLE:
                return "Pickable";

            case ObejctKind.MISSILE:
                return "Missile";

            case ObejctKind.EXPLOSION:
                return "Explosion";

            default:
                return null;
        }
    }

    private string GetParentName(ObejctKind KIND)
    {
        switch (KIND)
        {
            case ObejctKind.CITIZEN_M:
                return "Citizen_M";

            case ObejctKind.CITIZEN_W:
                return "Citizen_W";

            case ObejctKind.POLICE_BAT:
                return "Police_Bat";

            case ObejctKind.POLICE_PISTOL:
                return "Police_Pistol";

            case ObejctKind.ZOMBIE:
                return "Zombie";

            case ObejctKind.PICKABLE:
                return "Pickable";

            case ObejctKind.MISSILE:
                return "Missile";
                
            case ObejctKind.EXPLOSION:
                return "Explosion";

            default:
                return null;
        }
    }
}
