﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventChecker : MonoBehaviour
{
    public List<EnemyHealth> enemy = new List<EnemyHealth>();

    public BossCinemaEvent cinemaEvent;
    public TankEvent tankEvent;

    public CineCamFollow cam;

    private bool eventStart = false;

    private void Start()
    {
        StartCoroutine(Checker());
    }

    private IEnumerator Checker()
    {
        int deadCount = 0;
        while(!eventStart)
        {
            for(int i = 0; i < enemy.Count; i++)
            {
                if (enemy[i].isDead)
                {
                    deadCount++;
                }

                if(deadCount == enemy.Count)
                {
                    eventStart = true;
                    break;
                }
            }
            deadCount = 0;
            yield return new WaitForSeconds(0.1f);
        }
        if(eventStart)
        {
            cam.enabled = false;
            tankEvent.SendMessage("EventStarter", SendMessageOptions.DontRequireReceiver);
            cinemaEvent.SendMessage("EventStarter", SendMessageOptions.DontRequireReceiver);
        }
        yield break;
    }
}
