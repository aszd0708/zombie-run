﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    [Header("GameOver Obeject")]
    public GameObject gameOver;
    public Image gameOverIMG;
    public Text gameOverText;
    public Button main, game;

    [Header("Victory")]
    public GameObject victory;
    public Image victoryIMG;
    public Text victoryText;

    private void VictoryStart()
    {
        StartCoroutine(VictoryAnimation());
    }

    private IEnumerator VictoryAnimation()
    {
        victory.SetActive(true);
        victoryIMG.color = new Color(victoryIMG.color.r, victoryIMG.color.g, victoryIMG.color.b, 0);
        victoryText.color = new Color(victoryText.color.r, victoryText.color.g, victoryText.color.b, 0);
        victoryIMG.DOFade(1, 3.0f);
        yield return new WaitForSeconds(3.0f);
        victoryText.DOFade(1, 2.0f);
        yield return new WaitForSeconds(4.0f);
        // 메뉴 씬으로 넘어가는 명령어
        victoryText.DOFade(0, 2.0f);
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Main");
    }

    private void GameOverStart()
    {
        StartCoroutine(GameOverAnimation());
    }

    private IEnumerator GameOverAnimation()
    {
        gameOver.SetActive(true);
        gameOverIMG.color = new Color(gameOverIMG.color.r, gameOverIMG.color.g, gameOverIMG.color.b, 0);
        gameOverText.color = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0);
        main.gameObject.SetActive(false);
        game.gameObject.SetActive(false);
        gameOverIMG.DOFade(1, 2.0f);
        yield return new WaitForSeconds(2.0f);
        gameOverText.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1.0f);
        main.gameObject.SetActive(true);
        game.gameObject.SetActive(true);
    }

    public void StartThisScene()
    {
        SceneManager.LoadScene("Stage0");
    }

    public void MainScene()
    {
        SceneManager.LoadScene("Main");
    }
}