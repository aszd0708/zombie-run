﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneMove : MonoBehaviour
{
    public Image fade;
    public Menu menu;

    public void StageStart()
    {
        menu.isPause = false;
        StartCoroutine(StageScene());
    }

    public void ExitStart()
    {
        menu.isPause = false;
        StartCoroutine(Exit());
    }

    private IEnumerator StageScene()
    {
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Stage0");
    }

    public IEnumerator Exit()
    {
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        Application.Quit();
    }

    public void MainStart()
    {
        menu.isPause = false;
        StartCoroutine(MainView());
    }

    private IEnumerator MainView()
    {
        Time.timeScale = 1;
        fade.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Main");
    }
}
