﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterControl : MonoBehaviour
{
    public float Speed;
    public ThirdPersonCameraControl camController;
    public float damping;
    public enum State
    {
        IDLE, MOVE
    }

    public State state;

    float hor;
    float ver;


    void FixedUpdate ()
    {
        SetState();
        PlayerMovement();
        Angle();
    }

    void PlayerMovement()
    {
        hor = Input.GetAxis("Horizontal");
        ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }

    private void Angle()
    {
        switch(state)
        {
            case State.MOVE:
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, camController.camMoveX, 0), Time.deltaTime * damping);
                break;
        }
    }

    private void SetState()
    {
        if (hor != 0 || ver != 0)
            state = State.MOVE;

        else if (hor == 0 && ver == 0)
            state = State.IDLE;
    }
}