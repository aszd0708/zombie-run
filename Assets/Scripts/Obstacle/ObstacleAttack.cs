﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAttack : MonoBehaviour
{
    public float damage;

    void OnCollisionStay(Collision col)
    {
        if (col.transform.CompareTag("Zombie") || col.transform.CompareTag("Enemys"))
        {
            Debug.Log("때림");
            col.transform.SendMessage("GetDamageFromObstacle", damage, SendMessageOptions.DontRequireReceiver);
        }
    }
}
