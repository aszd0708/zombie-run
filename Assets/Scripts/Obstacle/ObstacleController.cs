﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public enum ObstacleKind
    {
        NIDDLE, FIRE, CUTTER
    }

    public ObstacleKind KIND;

    public bool isWork = true;

    [Header("0이하일때 작동 안함")]
    public float workTime;

    private Collider col;
    private Animation anime;
    private ParticleSystem fire;
    // Start is called before the first frame update

    void Awake()
    {
        col = GetComponent<Collider>();
        switch(KIND)
        {
            case ObstacleKind.CUTTER:
            case ObstacleKind.NIDDLE:
                anime = GetComponentInParent<Animation>();
                break;
            case ObstacleKind.FIRE:
                fire = GetComponentInChildren<ParticleSystem>();
                break;
        }
    }

    public void Stop()
    {
        switch(KIND)
        {
            case ObstacleKind.CUTTER:
            case ObstacleKind.NIDDLE:
                anime.enabled = false;
                break;
            case ObstacleKind.FIRE:
                fire.Stop();
                break;
        }
        col.enabled = false;
        isWork = false;
    }
}
