﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleStopButton : MonoBehaviour
{
    public List<ObstacleController> obstacles = new List<ObstacleController>();
    public float Ysize;

    private new Collider collider;

    void Awake()
    {
        collider = GetComponent<Collider>();
    }

    void OnCollisionStay(Collision col)
    {
        if(col.transform.CompareTag("Zombie"))
        {
            Debug.Log("좀비와 부딪힘");
            transform.position = new Vector3(transform.position.x, transform.position.y - Ysize, transform.position.z);
            collider.enabled = false;

            for(int i = 0; i < obstacles.Count; i++)
            {
                obstacles[i].Stop();
            }
        }
    }
}
