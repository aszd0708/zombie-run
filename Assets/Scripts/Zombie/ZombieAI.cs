﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieAI : MonoBehaviour
{
    public enum ZombieState
    {
        IDLE, MOVE, FOLLOWPLAYER, TOPLAYER, ATTACK
    }

    public ZombieState STATE;
    private Transform player;
    private ZombieMovement movement;
    private ZombieAttack attack;
    private NavMeshAgent navAgent;
    private ZombieHealth health;
    private AudioSource AS;    

    private void Awake()
    {
        movement = GetComponent<ZombieMovement>();
        attack = GetComponent<ZombieAttack>();
        health = GetComponent<ZombieHealth>();

        navAgent = GetComponent<NavMeshAgent>();
        AS = GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player").transform;
    }

    private void OnEnable()
    {
        STATE = ZombieState.FOLLOWPLAYER;
        StartCoroutine(CheckState());
    }

    private void Update()
    {
        if (health.isDead)
            return;

        switch(STATE)
        {
            case ZombieState.FOLLOWPLAYER:
                movement.FollowPlayer = true;
                break;
            case ZombieState.TOPLAYER:
                movement.ToPlayer = true;
                break;
            case ZombieState.ATTACK:
                attack.Attack = true;
                break;
        }
    }

    private IEnumerator CheckState()
    {
        while(true)
        {
            if(STATE == ZombieState.TOPLAYER)
            {
                float navPlayerDist = navAgent.remainingDistance;
                if (navPlayerDist < 5f)
                    STATE = ZombieState.FOLLOWPLAYER;
            }

            else if(STATE == ZombieState.FOLLOWPLAYER || STATE == ZombieState.IDLE)
            {
                navAgent.SetDestination(player.position);
                float navPlayerDist = navAgent.remainingDistance;
                if (navPlayerDist > 5.0f)
                    STATE = ZombieState.TOPLAYER;

                else
                    navAgent.isStopped = true;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SettingTarget(Transform target)
    {
        attack.SendMessage("SetTarget", target, SendMessageOptions.DontRequireReceiver);
        STATE = ZombieState.ATTACK;
    }
}
