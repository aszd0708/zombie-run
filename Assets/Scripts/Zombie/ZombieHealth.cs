﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHealth : MonoBehaviour
{
    public int maxHP;
    private int _HP;

    public int HP
    {
        get { return _HP; }
        set
        {
            _HP = value;
            hitEffect.Play();
            if (HP <= 0)
                Die();
        }
    }

    public bool isDead = false;

    private Animator animator;
    private PoolManager PM;
    private ZombiePickupAndThrow pickup;
    private BGMConroller bgm;

    private AudioManager AM;

    private bool first = false;

    private float obstacleTime = 0.5f;
    private float obstacleTimer;

    public ParticleSystem hitEffect;

    void Awake()
    {
        PM = FindObjectOfType<PoolManager>();
        animator = GetComponent<Animator>();
        pickup = GetComponent<ZombiePickupAndThrow>();

        bgm = GameObject.FindGameObjectWithTag("Player").GetComponent<BGMConroller>();

        AM = FindObjectOfType<AudioManager>();
    }

    void OnEnable()
    {
        isDead = false;
        _HP = maxHP;
        obstacleTimer = obstacleTime;
        foreach (Collider c in GetComponents<Collider>())
            c.enabled = true;
        if(first)
        {
            transform.parent.SendMessage("SetState", SendMessageOptions.DontRequireReceiver);
            bgm.VolumeUp();
        }
        first = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    void Update()
    {
        obstacleTimer += Time.deltaTime;
    }

    public void GetDamage(int damage)
    {
        if (!isDead)
            HP -= damage;
    }

    public void GetDamageFromObstacle(int damage)
    {
        if(!isDead)
            if(obstacleTime <= obstacleTimer)
            {
                obstacleTimer = 0;
                HP -= damage;
            }
    }

    private void Die()
    {
        // 죽은거 처리
        isDead = true;

        // 죽을때 물리에 관한 모든것 종료
        foreach (Collider c in GetComponents<Collider>())
            c.enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;

        // 들고있는거 없애줌
        pickup.SetDead();

        // 상속 제거함

        // 대충 죽는 모션
        animator.SetTrigger("IsDead");
        AM.PlaySound("zombieDead_sound", transform.position);
        StartCoroutine(AfterDie());
    }

    private IEnumerator AfterDie()
    {
        // 죽은 뒤 밑으로 시신이 내려가는 느낌 후 풀링
        yield return new WaitForSeconds(1.0f);
        //Destroy(gameObject);
        PM.SetPooledObject(gameObject, ObejctKind.ZOMBIE);
        yield break;
    }

    public int GetHP()
    {
        return _HP;
    }
}
