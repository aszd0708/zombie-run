﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieAttack : MonoBehaviour
{
    public Transform targets;
    public Transform target;
    public int damage;
    public float attackDist;
    public Transform player;

    // 공격 쿨타임
    public float attackCoolTime;
    // 쿨타임 돌리는 변수
    private float _attacTime;

    private NavMeshAgent navAgent;
    private Animator animator;

    private ZombieAI zombieAI;
    private AudioManager AM;
    private bool _attack;

    private AudioSource AS;
    public bool Attack
    {
        get { return _attack; }
        set
        {
            _attack = value;
            if(Attack)
            {
                ToTarget();
            }
        }
    }

    private void Awake()
    {
        player = GameObject.FindWithTag("Player").transform;

        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        
        zombieAI = GetComponent<ZombieAI>();

        AM = FindObjectOfType<AudioManager>();
        AS = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _attacTime = attackCoolTime;
    }

    private void Update()
    {
        _attacTime += Time.deltaTime;
    }

    // 세팅 타겟
    public void SetTarget(Transform targetT)
    {
        target = targetT;
    }
    // 타겟에 감 
    // nav 를 타겟에 맞추고 가까이 감
    private void ToTarget()
    {
        if (target == null)
        {
            return;
        }
        if(Input.GetMouseButton(0))
        {
            navAgent.SetDestination(target.position);
            float targetToDist = Vector3.Distance(transform.position, target.position);//navAgent.remainingDistance;
            if (targetToDist > attackDist)
            {
                navAgent.isStopped = false;
            }

            else if (targetToDist <= attackDist)
            {
                navAgent.isStopped = true;
                AttackTarget();
            }
        }

        else if(Input.GetMouseButtonUp(0))
        {
            zombieAI.STATE = ZombieAI.ZombieState.FOLLOWPLAYER;
        }
        transform.LookAt(target);
    }

    private void AttackTarget()
    {
        // 쿨타임이 다 돌았을 때
        if(_attacTime >= attackCoolTime)
        {
            navAgent.isStopped = true;
            animator.SetTrigger("IsAttack");
            target.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
            _attacTime = 0;
            AM.PlaySound("Target", transform.position);
        }
    }

    private void AttackDisForBoss()
    {
        attackDist = 5;
    }
}
