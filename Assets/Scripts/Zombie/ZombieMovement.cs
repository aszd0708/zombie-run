﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieMovement : MonoBehaviour
{
    public GameObject target;

    public float Speed;
    public float damping;
    public enum MoveState
    {
        IDLE, MOVE, LOOKAT
    }
    public MoveState MOVESTATE;

    float hor;
    float ver;
    private NavMeshAgent navMeshAgent;
    private Animator animator;

    private Vector3 lookDirect;
    private Transform player;

    private AudioManager AM;

    // 여기서 부터 AI 스크립트
    // 플레이어와 같은 움직임을 하는 변수 및 프로퍼티
    private bool _followPlayer;
    public bool FollowPlayer
    {
        get { return _followPlayer; }
        set
        {
            _followPlayer = value;
            if(FollowPlayer)
            {
                SetState();
                PlayerMovement();
                Angle();
            }
        }
    }

    // 플레이어와 멀어졌을 때 플레이어에게 오는 변수 및 프로퍼티
    private bool _toPlayer;
    public bool ToPlayer
    {
        get { return _toPlayer; }
        set
        {
            _toPlayer = value;
            if(ToPlayer)
            {
                ToPlayerMove();
            }
        }
    }
    
    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
        AM = FindObjectOfType<AudioManager>();
    }

    void OnEnable()
    {
        int random = Random.Range(0, 3);
        if(random == 0)
            StartCoroutine(PlayIdleSound());
    }

    private IEnumerator PlayIdleSound()
    {
        yield return new WaitForSeconds(Random.Range(5f, 20f));
        while(true)
        {
                AM.PlaySound("zombieIdle1_sound", transform.position);
            yield return new WaitForSeconds(Random.Range(20f, 30f));
        }
    }

    void PlayerMovement()
    {
        hor = Input.GetAxis("Horizontal");
        ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
        lookDirect = ver * Vector3.forward + hor * Vector3.right ;
    }

    private void Angle()
    {
        switch (MOVESTATE)
        {
            case MoveState.MOVE:
                transform.rotation = Quaternion.Lerp(transform.rotation, player.rotation, Time.deltaTime * damping);
                //transform.rotation = Quaternion.LookRotation(lookDirect);
                //player.rotation
                animator.SetBool("IsRunning", true);
                break;
            case MoveState.IDLE:
                animator.SetBool("IsRunning", false);
                break;
            case MoveState.LOOKAT:
                break;
        }
    }

    private void SetState()
    {
        if (hor != 0 || ver != 0)
            MOVESTATE = MoveState.MOVE;

        else if (hor == 0 && ver == 0)
            MOVESTATE = MoveState.IDLE;
    }

    private void Attack()
    {
        // 타겟까지 간 뒤에 공격하기 (공격은 범위 안에 들어왔을 때 공격 후 쿨타임 돌림)
        // 이건 다른 스크립트에서 쓰는걸로
        navMeshAgent.SetDestination(target.transform.position);
        navMeshAgent.isStopped = false;
    }

    private void ToPlayerMove()
    {
        // 플레이어의 일정 범위까지 왔을 경우 ActionState 를 FOLLOWPLAYER로 변경
        animator.SetBool("IsRunning", true);
        navMeshAgent.SetDestination(player.position);
        navMeshAgent.isStopped = false;

        if (Vector3.Distance(player.position, transform.position) >= 100f)
            transform.position = player.position;

        float playerDistacne = navMeshAgent.remainingDistance;
    }

    private void CheckPlayerDistance()
    {
        // 만약 ActionState가 FOLLOWPLAYER일때 검사 일정 거리 멀어지면 ActionState를 TOPLAYER로 변경
        navMeshAgent.SetDestination(player.position);
        float playerDistacne = navMeshAgent.remainingDistance;
    }
}