﻿using UnityEngine;

public class ZombiePickupAndThrow : MonoBehaviour
{
    public Transform hand;
    public int pickupDamage;

    // 충돌 한 모든 오브젝트를 손에 상속 시키고 trasnfrom 초기화
    // 충돌 한 오브젝트는 따로 스크립트를 State값 넣어서 관리
    // 일단 이 스크립트에서는 있다 없다만 넣기
    // 그런데 bool보다는 enum으로 만들어서 넣는게 좀 더 괜찮아보임

    public enum State
    {
        PICKUP, NOTHING
    }
    public State STATE;

    // 던지는 힘!
    public float throwPower;
    public LayerMask layerMask;

    private Transform item;

    private ZombieAttack attack;

    // 부모에서 한번에 제어할수 있게 할당 받음
    private ZombieThrowable commander;

    private AudioManager AM;

    void Awake()
    {
        attack = GetComponent<ZombieAttack>();
        commander = FindObjectOfType<ZombieThrowable>();
        AM = FindObjectOfType<AudioManager>();
    }

    void OnEnable()
    {
        STATE = State.NOTHING;
    }

    void OnCollisionEnter(Collision col)
    {
        switch (STATE)
        {
            case State.NOTHING:
                if (col.gameObject.CompareTag("Pickable"))
                {
                    // 여기 테그 이름 적고 오브젝트가 충돌 하면 손에 넣기
                    // 함수 써서 넣기 그리고 스테이드 바꾸기
                    PickupObj(col.transform.gameObject);
                }
                break;

            case State.PICKUP:
                // 밀치기 나무나 가로등이나 쓰러지게
                DownObj(col.transform.gameObject);
                break;
        }
    }

    // 죽었을때 오브젝트도 false해주는 함수 만들어서 쓰기

    private void PickupObj(GameObject obj)
    {
        // 여기 obj 파일 만들어서 함수 호출 (픽업 상태로)
        STATE = State.PICKUP;
        commander.GetPickup(this);
        item = obj.transform;
        obj.SendMessage("InHand", SendMessageOptions.DontRequireReceiver);
        obj.transform.SetParent(hand);
        obj.transform.localPosition = new Vector3(0, 0, 0);
        attack.damage += pickupDamage;
    }

    private void DownObj(GameObject obj)
    {
        obj.SendMessage("Down", SendMessageOptions.DontRequireReceiver);
    }

    // 여기서 부터 포물선 투척

    public void ThrowItem()
    {
        if (STATE == State.NOTHING)
            return;
        AM.PlaySound("AirSwoosh", transform.position);
        STATE = State.NOTHING;
        attack.damage -= pickupDamage;
        // 투척한 아이템 상태 변화
        Debug.Log(item.transform.position);
        item.transform.localPosition = (new Vector3(item.transform.localPosition.x, 1.0f, item.transform.localPosition.z));
        item.gameObject.SendMessage("Throw", SendMessageOptions.DontRequireReceiver);
        // 투척 아이템의 rigid값 불러옴
        Rigidbody itemRigid = item.GetComponent<Rigidbody>();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        Vector3 throwAngle;
        if (Physics.Raycast(ray, out rayHit, Mathf.Infinity, ~16))
        {
            throwAngle = rayHit.point - item.transform.position;
        }
        else
        {
            throwAngle = item.transform.forward * 1f;
        }
        throwAngle.y = 3f;
        itemRigid.AddForce(throwAngle * throwPower, ForceMode.Impulse);
        // 이 클래스에 할당된 아이템을 없애줌
        item = null;
    }

    public void SetDead()
    {
        switch(STATE)
        {
            case State.PICKUP:
                item.SendMessage("SetNothing", SendMessageOptions.DontRequireReceiver);
                break;
            case State.NOTHING:
                return;
        }
    }
}
