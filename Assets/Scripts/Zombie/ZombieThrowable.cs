﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieThrowable : MonoBehaviour
{
    private List<ZombiePickupAndThrow> pickupZombies = new List<ZombiePickupAndThrow>();
    private AudioManager AM;

    private void Awake()
    {
        AM = FindObjectOfType<AudioManager>();
    }

    public void GetPickup(ZombiePickupAndThrow pickup)
    {
        pickupZombies.Add(pickup);
    }

    public void DelPickup(ZombiePickupAndThrow pickup)
    {
        pickupZombies.Remove(pickup);
    }

    public void CommandThrow()
    {
        if (pickupZombies.Count <= 0)
            return;
        int randomIndex = Random.Range(0, pickupZombies.Count);
        pickupZombies[randomIndex].ThrowItem();
        DelPickup(pickupZombies[randomIndex]);
    }
}
