﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieCountBar : MonoBehaviour
{
    public SimpleHealthBar zombieCountGauage;
    public Text zombieCountNumber;

    public int maxCount;
    private int _zombieCount;

    void Start()
    {
        SetState();
    }

    private void MinZombie()
    {

    }

    private void AddZombie()
    {

    }

    private void SetState()
    {
        _zombieCount = transform.childCount;
        zombieCountGauage.UpdateBar(_zombieCount, maxCount);
        if (_zombieCount < 10)
            zombieCountNumber.text = "X" + _zombieCount + " ";
        else
            zombieCountNumber.text = "X" + _zombieCount;
    }
}
