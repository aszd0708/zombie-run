﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarFollowMouse : MonoBehaviour
{
    public Canvas parentCanvas;
    public bool isTarget;

    private Image img;

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    private void Start()
    {
        Vector2 pos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform, Input.mousePosition,
            parentCanvas.worldCamera,
            out pos);
    }

    public void Update()
    {
        Vector2 movePos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            Input.mousePosition, parentCanvas.worldCamera,
            out movePos);

        transform.position = parentCanvas.transform.TransformPoint(new Vector2(movePos.x + 80, movePos.y));
    }

    public void SetTargetBool(bool value)
    {
        isTarget = value;
        if(!isTarget)
            img.enabled = false;
    }

    public void FillHealth(EnemyHealth health)
    {
        if(isTarget)
        {
            img.enabled = true;
            // 타겟의 체력을 fill로 표시
            float healthPer = ((float)health.GetHP() / (float)health.maxHP);
            img.fillAmount = healthPer;
        }
    }
}
