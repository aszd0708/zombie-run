﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float Speed;
    public ThirdPersonCameraControl camController;
    public float damping;
    public Transform zombies;
    public enum State
    {
        IDLE, MOVE
    }

    public State state;
    public bool attack;

    float hor;
    float ver;

    public ZombieThrowable throwCommander;

    public Image countBar;

    public SimpleHealthBar zombieCountBar;
    public Text zombieCounterNumber;

    private Vector3 centerPos;

    private PoolManager PM;

    private bool isPlay = true;

    public GameOver over;

    void Awake()
    { 
        PM = FindObjectOfType<PoolManager>();
    }

    void Start()
    {
        //StartCoroutine("CenterPosition");
        //PM.GetPooledObject(EnemyKinds.ZOMBIE);
        //StartCoroutine(PosSetting());
    }

    void Update()
    {
        int zombieCounter = zombies.childCount;
        zombieCountBar.UpdateBar(zombieCounter, 30);
        if (zombieCounter < 10)
            zombieCounterNumber.text = "X" + zombieCounter + " ";
        else
            zombieCounterNumber.text = "X" + zombieCounter;
    }

    void FixedUpdate()
    {
        if (!isPlay)
            return;

        SetState();
        PlayerMovement();
        Angle();
        Throw();
    }
    
    void Throw()
    {
        if (Input.GetKeyDown(KeyCode.F))
            throwCommander.CommandThrow();
    }

    void PlayerMovement()
    {
        if (zombies.childCount <= 0)
        {
            SetPlay(false);
            Debug.Log("끝");
            over.SendMessage("GameOverStart", SendMessageOptions.DontRequireReceiver);
            this.enabled = false;
            return;
        }

        hor = Input.GetAxis("Horizontal");
        ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
        transform.position = Vector3.Lerp(transform.position, GetSenterPos(), Time.deltaTime * 2f);
    }

    IEnumerator PosSetting()
    {
        while(true)
        {
            transform.position = Vector3.Lerp(transform.position, GetSenterPos(), Time.deltaTime * 3f);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void Angle()
    {
        switch (state)
        {
            case State.MOVE:
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, camController.camMoveX, 0), Time.deltaTime * damping);
                //zom.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, camController.mouseX, 0), Time.deltaTime * damping);
                break;
        }
    }

    private void SetState()
    {
        if (hor != 0 || ver != 0)
            state = State.MOVE;

        else if (hor == 0 && ver == 0)
            state = State.IDLE;
    }

    private IEnumerator CenterPosition()
    {
        // 5초에 한번씩 좀비들의 중앙에 있는 곳으로 옮기는 함수
        while(true)
        {
            List<Transform> zombiesTrans = new List<Transform>();
            centerPos = new Vector3(0, 0, 0);
            for (int i = 0; i < zombies.childCount; i++)
            {
                zombiesTrans.Add(zombies.GetChild(i));
                centerPos += zombies.GetChild(i).transform.position;
            }
            centerPos /= zombies.childCount;
            transform.DOMove(centerPos, 0.5f);
            yield return new WaitForSeconds(5.0f);
        }
    }

    public void SetZombiesTarget(Transform target)
    {
        for(int i = 0; i < zombies.childCount; i++)
            zombies.GetChild(i).SendMessage("SettingTarget", target, SendMessageOptions.DontRequireReceiver);
        attack = true;
        if (Input.GetMouseButtonUp(0))
        {
            attack = false;
        }
    }

    public Vector3 GetSenterPos()
    {
        // 좀비들중 중앙값을 반환하는 함수
        if (zombies.childCount == 1)
            return zombies.GetChild(0).transform.position;
        List<Transform> zombiesTrans = new List<Transform>();
        Vector3 centerPos = new Vector3(0, 0, 0);
        for (int i = 0; i < zombies.childCount; i++)
        {
            zombiesTrans.Add(zombies.GetChild(i));
            centerPos += zombies.GetChild(i).transform.position;
        }
        centerPos /= zombies.childCount;
        transform.DOMove(centerPos, 0.5f);
        return centerPos;
    }

    private void SetPlay(bool value)
    {
        isPlay = value;
    }

    public void ZombieAttackDisUp()
    {
        for(int i = 0; i < zombies.childCount; i++)
        {
            zombies.GetChild(i).SendMessage("AttackDisForBoss", SendMessageOptions.DontRequireReceiver);
        }
    }

}
