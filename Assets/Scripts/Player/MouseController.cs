﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public Texture2D attackCursor;
    public Texture2D attackCursor_R;
    public Texture2D nullCursor;
    public CursorMode cursorMode = CursorMode.Auto;
    public LayerMask mask;
    public HealthBarFollowMouse healthBar;
    public PlayerController PC;

    public bool isEvent;
    private Vector2 hotSpot;

    void Awake()
    {
        PC = GetComponent<PlayerController>();
    }

    void Start()
    {
        hotSpot = new Vector2(attackCursor.width / 2, attackCursor.height / 2);
    }

    void Update()
    {
        if (isEvent)
            return;
        RaycastHit hit = new RaycastHit();
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //healthBar.position = new Vector3(mousePos.x, mousePos.y, -Camera.main.transform.position.z);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.collider.tag == "Enemys")
            {
                Vector3 hitPos = hit.transform.position;
                healthBar.SetTargetBool(true);
                healthBar.FillHealth(hit.transform.GetComponent<EnemyHealth>());
                if (Input.GetMouseButton(0))
                {
                    PC.SetZombiesTarget(hit.transform);
                    Cursor.SetCursor(attackCursor_R, hotSpot, cursorMode);
                }
                else
                    Cursor.SetCursor(attackCursor, hotSpot, cursorMode);
            }

            else if(hit.collider.CompareTag("Tank"))
            {
                Vector3 hitPos = hit.transform.position;
                Cursor.SetCursor(attackCursor, hotSpot, cursorMode);
                if (Input.GetMouseButton(0))
                {
                    PC.SetZombiesTarget(hit.transform);
                    Cursor.SetCursor(attackCursor_R, hotSpot, cursorMode);
                }
            }

            else
            {
                healthBar.SetTargetBool(false);
                Cursor.SetCursor(nullCursor, hotSpot, cursorMode);
                healthBar.FillHealth(hit.transform.GetComponent<EnemyHealth>());
            }
        }
    }
}
