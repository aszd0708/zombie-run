﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObjectState : MonoBehaviour
{
    public enum State
    {
        STAND, DOWN, INHAND, USED, THROW
    }

    public enum Kind
    {
        DEFAULT, EXPLOSION
    }

    public State STATE;
    public Quaternion rot;
    public int damage;

    public Kind KIND;

    private Rigidbody rigid;

    private List<Collider> col = new List<Collider>();
    private PoolManager PM;
    private AudioManager AM;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        PM = FindObjectOfType<PoolManager>();
        AM = FindObjectOfType<AudioManager>();
        foreach (Collider c in GetComponents<Collider>())
        {
            if (c.isTrigger)
                continue;
            col.Add(c);
        }
    }

    private void OnEnable()
    {
        STATE = State.STAND;
    }

    void Update()
    {
        if (STATE == State.THROW)
        {
            if (rigid.velocity.x <= 0.5f && rigid.velocity.y <= 0.5f && rigid.velocity.z <= 0.5f)
            {
                STATE = State.USED;
                StartCoroutine(Pooling());
            }
        }
        else if (STATE == State.INHAND)
            transform.localPosition = Vector3.zero;
    }

    private void InHand()
    {
        STATE = State.INHAND;
        for (int i = 0; i < col.Count; i++)
            col[i].enabled = false;
        gameObject.layer = 11;
        rigid.constraints = RigidbodyConstraints.FreezePositionY;
        transform.rotation = rot;
    }

    private void Throw()
    {
        Debug.Log("던짐 상태");
        gameObject.tag = "Untagged";
        rigid.constraints = RigidbodyConstraints.None;
        STATE = State.THROW;
        transform.SetParent(null);
        for (int i = 0; i < col.Count; i++)
            col[i].enabled = true;
    }

    private void Down()
    {
        STATE = State.DOWN;
        rigid.freezeRotation = false;
        transform.SetParent(null);
        //StartCoroutine(Pooling());
    }

    private void SetNothing()
    {
        STATE = State.DOWN;
        for (int i = 0; i < col.Count; i++)
            col[i].enabled = true;
        rigid.constraints = RigidbodyConstraints.None;
        rigid.freezeRotation = false;
        transform.SetParent(null);
        StartCoroutine(Pooling());
    }

    private IEnumerator Pooling()
    {
        yield return new WaitForSeconds(1.5f);
        transform.DOMoveY(-5f, 3.0f);
        yield return new WaitForSeconds(3.0f);
        if(KIND == Kind.EXPLOSION)
        PM.SetPooledObject(gameObject, ObejctKind.EXPLOSION);

        else
        PM.SetPooledObject(gameObject, ObejctKind.PICKABLE);
    }


    // throw 상태일때 던짐 -> 데미지를 줌

    //void OnTriggerEnter(Collider other)

    void OnCollisionEnter(Collision other)
    {
        switch(STATE)
        {
            case State.THROW:
                if (other.transform.CompareTag("Enemys"))
                {
                    other.transform.SendMessage("GetDamage", damage);
                    AM.PlaySound("HitClub03", transform.position);
                }
                if (KIND == Kind.EXPLOSION)
                {
                    // 대충 터지는 스크립트 
                    if(!other.transform.CompareTag("Zombie"))
                    {
                        gameObject.SendMessage("Explosion", SendMessageOptions.DontRequireReceiver);
                        AM.PlaySound("BlockMetalMetal04", transform.position);
                    }
                }
                // 여기에???
                break;
        }
    }
}
