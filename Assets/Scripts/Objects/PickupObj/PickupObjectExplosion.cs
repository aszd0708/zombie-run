﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PickupObjectExplosion : MonoBehaviour
{
    public ParticleSystem particle;

    public int damage;
    public float explosionRadius;

    private bool isExplosion;

    private AudioManager AM;

    private void Awake()
    {
        AM = FindObjectOfType<AudioManager>();
    }

    private void OnEnable()
    {
        GetComponent<MeshRenderer>().material.color = Color.white;
    }


    private void Explosion()
    {
        if (isExplosion)
            return;
        isExplosion = true;
        particle.transform.position = transform.position;
        particle.Play();
        // 색 변경 까맣게
        GetComponent<MeshRenderer>().material.color = Color.black;
        Debug.Log("터짐");
        ShakeCam();
        foreach (Collider other in Physics.OverlapSphere(transform.position, explosionRadius))
        {
            if (other.gameObject.tag == "Zombie")
                continue;
            other.gameObject.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
        AM.PlaySound("Explosion", transform.position);
    }

    private void ShakeCam()
    {
        Transform camTransform = Camera.main.transform;
        camTransform.DOShakePosition(0.5f, 3, 25, 50);
    }
}
