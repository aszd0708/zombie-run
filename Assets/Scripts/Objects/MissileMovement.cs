﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMovement : MonoBehaviour
{

    public int damage;
    public float speed;
    public ParticleSystem explosionEffect;
    public float explosionRadius;
    public Transform front;
    private PoolManager PM;
    private AudioManager AM;
    private Transform player;
    private Vector3 playerPos;
    int count = 0;
    void Awake()
    {
        PM = FindObjectOfType<PoolManager>();
        AM = FindObjectOfType<AudioManager>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void OnEnable()
    {
        count++;
        playerPos = player.position;
        transform.LookAt(new Vector3(player.position.x, player.position.y + 5, player.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += front.forward * speed;
    }

    void OnCollisionEnter(Collision other)
    {
        GameObject effect = Instantiate(explosionEffect.gameObject);
        AM.PlaySound("Explosion", transform.position);
        effect.transform.position = transform.position;
        foreach (Collider others in Physics.OverlapSphere(transform.position, explosionRadius))
        {
            others.gameObject.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
        PM.SetPooledObject(gameObject, ObejctKind.MISSILE);
        if(count > 3)
        {
            Transform camTransform = Camera.main.transform;
            camTransform.DOShakePosition(0.5f, 3, 25, 50);
        }
        else
        {
            Transform cineCam = GameObject.FindGameObjectWithTag("Cinecam").transform;
            cineCam.DOShakePosition(0.5f, 3, 25, 50);
        }

        if (other.transform.CompareTag("EventWall"))
            other.gameObject.SetActive(false);
    }
}
