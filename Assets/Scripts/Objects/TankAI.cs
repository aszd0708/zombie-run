﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    public enum State
    {
        MOVEMENET, ATTACK, IDLE, GROGI
    }

    public State STATE;

    private TankMovement movemenet;
    private TankAttack attack;
    private TankHealth health;

    private void Awake()
    {
        movemenet = GetComponent<TankMovement>();
        attack = GetComponent<TankAttack>();
        health = GetComponent<TankHealth>();
    }

    private void Update()
    {
        if (health.isStop || health.isDead)
            return;

        switch(STATE)
        {
            case State.MOVEMENET:
                movemenet.Movement = true;
                break;
            case State.ATTACK:
                movemenet.Movement = false;
                break;
            case State.IDLE:
                return;
            case State.GROGI:
                movemenet.Movement = false;
                break;
        }
        Debug.Log(STATE);
    }

    private void StartAttack()
    {
        StartCoroutine(AttackAI());
    }

    private IEnumerator AttackAI()
    {
        STATE = State.ATTACK;
        attack.SendMessage("AttackStarter", SendMessageOptions.DontRequireReceiver);
        yield return new WaitForSeconds(attack.GetAttackTime());
        STATE = State.MOVEMENET;
    }

    private void SetDeadState()
    {
        movemenet.enabled = false;
        attack.enabled = false;
    }
}
