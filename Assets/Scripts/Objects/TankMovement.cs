﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankMovement : MonoBehaviour
{
    // 전체적인 위치 저장
    public List<Transform> movePos = new List<Transform>();

    // 이동시 주는 데미지
    public int damage;

    // 패턴을 구별하기 위해 넣음
    public bool moveEnd;

    // 현재 이동하고 있는 위치 저장
    private Transform currentTransform;
    private TankHealth health;
    private NavMeshAgent navAgent;

    // 움직이고 있다면 충돌하는 순간 데미지를 주기 위해 넣어놓음
    private bool isMove = false;

    // movemenet 프로퍼티
    private bool _move;
    public bool Movement
    {
        get { return _move; }
        set
        {
            _move = value;
            if (Movement)
            {
                navAgent.isStopped = false;
                Move();
            }
            else
            {
                navAgent.isStopped = true;
                isMove = false;
            }
            Debug.Log(navAgent.isStopped);
        }
    }

    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        health = GetComponent<TankHealth>();
    }

    void OnEnable()
    {
        currentTransform = RandomPos();
    }

    private Transform RandomPos()
    {
        if (currentTransform == null)
            return movePos[Random.Range(0, movePos.Count)];
        else
        {
            Transform valueTransform = movePos[Random.Range(0, movePos.Count)];
            movePos.Remove(valueTransform);
            movePos.Add(currentTransform);
            return valueTransform;
        }
    }

    private void Move()
    {
        Debug.Log(navAgent.isStopped);
        if (health.isStop)
        {
            navAgent.isStopped = true;
            isMove = false;
            return;
        }
        moveEnd = false;
        isMove = true;
        // 도착지점 설정
        navAgent.SetDestination(currentTransform.position);
        navAgent.isStopped = false;

        // 만약 도착지점에 오면 currentTransform을 바꿔준뒤 공격하는 함수 실행 하지만 실행하는 함수는 AI에서 다룰 예정
        if (Vector3.Distance(transform.position, currentTransform.position) <= 0.5f)
        {
            moveEnd = true;
            isMove = false;
            // 대충 여기에 AI의 STATE를 변경하는 명령어를 넣는다.
            currentTransform = RandomPos();
            gameObject.SendMessage("StartAttack", SendMessageOptions.DontRequireReceiver);
            //StartCoroutine(MoveEndMotion());
        }
    }

    private IEnumerator MoveEndMotion()
    {
        Vector3 currentPos = transform.position;
        Vector3 currentRot = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
        transform.DORotate(new Vector3(transform.rotation.x + 10f, transform.rotation.y, transform.rotation.z), 0.5f).SetEase(Ease.OutCirc);
        transform.DOMove(transform.position + transform.forward * 3, 0.5f).SetEase(Ease.OutCirc);
        yield return new WaitForSeconds(0.5f);
        transform.DORotate(currentRot, 0.25f).SetEase(Ease.OutCirc);
        transform.DOMove(currentPos, 0.5f).SetEase(Ease.OutCirc).SetEase(Ease.InQuad);
        yield break;
    }

    void OnCollisionEnter(Collision other)
    {
        if (isMove)
            if (other.transform.CompareTag("Zombie"))
                other.transform.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
    }

    public void FalseIsMove()
    {
        isMove = false;
    }
}
