﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour
{
    public bool isStop = false;
    public SimpleHealthBar healthBar;
    public SimpleHealthBar ammorBar;
    public GameObject doubleBar;

    private TankAI AI;

    public List<ParticleSystem> explosionEffect = new List<ParticleSystem>();

    public int maxHP;

    public GameOver victory;
    private int _HP;
    public int HP
    {
        get { return _HP; }
        set
        {
            _HP = value;
            
            if(HP <= 0)
            {
                // 대충 죽는 함수
                SetDead();
            }
        }
    }
    public int maxAmmor;
    private int _ammor;
    public int Ammor
    {
        get { return _ammor; }
        set
        {
            _ammor = value;
            if(Ammor <= 0)
            {
                // 대충 그로기에 빠지는 함수
                SetGroggy();
            }
        }
    }

    public bool isGroggy = false;
    public bool isDead = false;

    public Transform head;

    public float groggyTime;

    private AudioManager AM;

    void Awake()
    {
        AI = GetComponent<TankAI>();
        AM = FindObjectOfType<AudioManager>();
    }

    void OnEnable()
    {
        HP = maxHP;
        Ammor = maxAmmor;
        doubleBar.SetActive(true);
    }

    void Update()
    {
        healthBar.UpdateBar(HP, maxHP);
        ammorBar.UpdateBar(Ammor, maxAmmor);
    }

    private void GetDamage(int damage)
    {
        if (isGroggy)
            HP -= damage;
        else
            Ammor -= damage;
    }

    private void SetGroggy()
    {
        StartCoroutine(Groggy());
    }

    private IEnumerator Groggy()
    {
        AI.STATE = TankAI.State.GROGI;
        yield return new WaitForSeconds(0.5f);
        isGroggy = true;
        isStop = true;
        Vector3 currentRot = new Vector3(head.localRotation.x, head.localRotation.y, head.localRotation.z);
        head.DOLocalRotate(new Vector3(15, 30, 0), 1f);
        yield return new WaitForSeconds(groggyTime);
        isGroggy = false;
        head.DOLocalRotate(currentRot, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Ammor = maxAmmor;
        isStop = false;
        AI.STATE = TankAI.State.MOVEMENET;
    }

    private void SetDead()
    {
        StartCoroutine(Dead());
    }

    private IEnumerator Dead()
    {
        isStop = true;
        isDead = true;
        head.DORotate(new Vector3(15, 30, 0), 1f);
        gameObject.SendMessage("SetDeadState", SendMessageOptions.DontRequireReceiver);
        // 대충 여기 터지는 이펙트
        for (int i = 0; i < explosionEffect.Count; i++)
        {
            explosionEffect[i].Play();
            AM.PlaySound("Explosion", transform.position);
        }
        yield return new WaitForSeconds(2.0f);
        victory.SendMessage("VictoryStart", SendMessageOptions.DontRequireReceiver);
    }
}
