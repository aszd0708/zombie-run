﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PoliceCarMovement : MonoBehaviour
{
    public Transform target;

    public Transform out_R, out_L;

    public Transform firstPos;

    private PoolManager PM;

    void Awake()
    {
        PM = FindObjectOfType<PoolManager>();
    }

    void Start()
    {
        StartCoroutine(DriftMovement());
    }

    void Update()
    {
        //transform.LookAt(transform.forward);
    }

    private IEnumerator DriftMovement()
    {
        ArrayList info = new ArrayList();
        info.Add(0);
        info.Add(firstPos);
        transform.DOMove(new Vector3(target.position.x, transform.position.y, target.position.z), 2.0f).SetEase(Ease.OutQuad);
        yield return new WaitForSeconds(1f);
        transform.DORotate(new Vector3(0, 120, 0), 0.5f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);
        GameObject police = PM.GetPooledObject(ObejctKind.POLICE_PISTOL);
        police.transform.position = out_R.position;
        police.SendMessage("SetFirstState", info, SendMessageOptions.DontRequireReceiver);
        yield return new WaitForSeconds(0.1f);
        GameObject police1 = PM.GetPooledObject(ObejctKind.POLICE_PISTOL);
        police1.transform.position = out_R.position;
        police1.SendMessage("SetFirstState", info, SendMessageOptions.DontRequireReceiver);
    }
}
