﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAttack : MonoBehaviour
{
    public Transform player;
    public Transform tankHead;
    public Transform muzzle;

    // 포를 쏘기 전까지 기다리는 시간.
    public float firstWaitTime;
    // 포를 쏘고 기다리는 시간
    public float secondWatiTime;

    private PoolManager PM;
    private AudioManager AM;

    private bool isFollow = true;
    private TankHealth health;

    void Awake()
    {
        PM = FindObjectOfType<PoolManager>();
        health = GetComponent<TankHealth>();
        AM = FindObjectOfType<AudioManager>();
    }
    // Update is called once per frame
    void Update()
    {
        if(isFollow && !health.isStop)
        {
            tankHead.LookAt(player.position);
        }
    }

    private void SetIsFollow(bool value)
    {
        isFollow = value;
    }

    private void AttackStarter()
    {
        StartCoroutine(Attack());
    }

    public float GetAttackTime()
    {
        return firstWaitTime + secondWatiTime;
    }

    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(firstWaitTime);
        // 대충 포를 쏘는 함수
        AM.PlaySound("FireBallStart", tankHead.position);
        if (health.isStop)
            yield break;
        GameObject missile = PM.GetPooledObject(ObejctKind.MISSILE, muzzle.position);
        missile.transform.LookAt(new Vector3(player.position.x, player.position.y + 0.5f, player.position.z));
        Debug.Log(muzzle.position);
        //missile.transform.position = muzzle.position;
        yield return new WaitForSeconds(secondWatiTime);
        // 대충 여기에 AI의 STATE를 변경하는 명령어를 넣는다.
    }

    private void EventAttackStarter(Vector3 targetPos)
    {
        StartCoroutine(EventAttack(targetPos));
    }

    private IEnumerator EventAttack(Vector3 targetPos)
    {
        GameObject missile = PM.GetPooledObject(ObejctKind.MISSILE, muzzle.position);
        missile.transform.LookAt(targetPos);
        //missile.transform.position = muzzle.position;
        yield return new WaitForSeconds(2.0f);
    }
}
