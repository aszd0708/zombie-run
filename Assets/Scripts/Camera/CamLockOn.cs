﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamLockOn : MonoBehaviour
{
    public float checkRange;
    public LayerMask checkMask;

    public GameObject focusedGameObject = null;

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, checkRange, checkMask))
        {
            Debug.Log(focusedGameObject.name);
            if (focusedGameObject != hit.transform.gameObject)
            {
                // 포커스 아웃된 신호 를 보내주고
                focusedGameObject.SendMessage("OnFocusExit", SendMessageOptions.DontRequireReceiver);
                    // 바꾸고
            }
            focusedGameObject = hit.transform.gameObject;
            focusedGameObject.SendMessage("OnFocusEnter", SendMessageOptions.DontRequireReceiver);
            // 포커스 된 곳에 포커스가 됐다고 신호를 보내줌


            
            hit.transform.GetComponent<LockOnMarkPos>().check = true;
        }
    }
}
