﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOnMarkPos : MonoBehaviour
{
    public GameObject lockOnMark;
    public bool check;

    private Camera mainCam;

    void Awake()
    {
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        lockOnMark.transform.position = transform.position;
        lockOnMark.transform.LookAt(lockOnMark.transform.position + mainCam.transform.rotation * Vector3.forward, mainCam.transform.rotation * Vector3.up);
    }

    private void OnFocusEnter()
    {
        check = true;
        lockOnMark.transform.localScale = new Vector2(2, 2);
    }

    private void OnFocusExit()
    {
        check = false;
        lockOnMark.transform.localScale = new Vector2(1, 1);
    }
    
}
