﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCameraControl : MonoBehaviour
{
    public float rotationSpeed = 1;
    public Transform Target, Player;
    public float mouseY;
    public float camMoveX = 0;

    public Transform Obstruction;
    float zoomSpeed = 2f;
    Vector3 targetDis;
    
    void Start()
    {
        targetDis = Target.position - transform.position;
        Obstruction = Target;
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        CamControl();
        //MoveCam();
        //ViewObstructed();
    }
    
    void MoveCam()
    {
        transform.position = Target.position - targetDis;
    }

    void CamControl()
    {
        if (Input.GetKey(KeyCode.Q))
            camMoveX += 1 * rotationSpeed;
        else if (Input.GetKey(KeyCode.E))
            camMoveX -= 1 * rotationSpeed;
        if (Input.GetMouseButton(1))
        {
            camMoveX += Input.GetAxis("Mouse X") * rotationSpeed;
            mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
            mouseY = Mathf.Clamp(mouseY, -31, 80);
        }
        transform.LookAt(Target);
        Target.rotation = Quaternion.Euler(mouseY, camMoveX, 0);
    }

    void ViewObstructed()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, Target.position - transform.position, out hit, 4.5f))
        {
            if (hit.collider.gameObject.tag != "Player")
            {
                Obstruction = hit.transform;
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                
                if(Vector3.Distance(Obstruction.position, transform.position) >= 3f && Vector3.Distance(transform.position, Target.position) >= 1.5f)
                    transform.Translate(Vector3.forward * zoomSpeed * Time.deltaTime);
            }
            else
            {
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                if (Vector3.Distance(transform.position, Target.position) < 4.5f)
                    transform.Translate(Vector3.back * zoomSpeed * Time.deltaTime);
            }
        }
    }
}