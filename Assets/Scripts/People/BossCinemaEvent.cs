﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BossCinemaEvent : MonoBehaviour
{
    // 각자 역할 배분
    /*
     * 0번 처음에 플레이어 위치와 회전을 받아와서 그대로 있음
     * 부드럽게 바꿈
     * 1번 플레이어 위치에서 lookat값을 벽으로 바꿔줌
     * 캠 바뀔때 서서히 바뀌는게 아니고 그냥 바로 바꿈
     * 2번 탱크 전방에서 총구를 lookat값으로 받음
     * 캠 그냥 바로 바꿈
     * 3번 탱크쪽에서 다른 벽을 lookat값으로 받음
     * 캠 바로 바꿈
     * 4번 3번 끝나고 다른벽이 터지면 목적지하고 탱크 사이에 밑에서 탱크를 lookat값으로 받아옴
     * 캠 바로 바꿈
     * 5번 목적지에서 살짝 멀리 둔 뒤 탱크를 lookat으로 받아옴
     * 부드럽게 바꿈
     * 0번 캠으로 이동
     */
    public Transform playerCam;
    public CinemachineVirtualCamera[] cinema = new CinemachineVirtualCamera[6];

    public Camera player;
    public Camera cine;

    public PlayerController controller;

    public MouseController cam;

    public BossBarrelSpawner spawner;

    private void Update()
    {
        cinema[0].transform.position = playerCam.position;
        cinema[0].transform.rotation = Quaternion.Euler(playerCam.rotation.x, playerCam.rotation.y, playerCam.rotation.z);
    }

    public void EventStarter()
    {
        cinema[0].transform.position = playerCam.position;
        cinema[0].transform.rotation = Quaternion.Euler(playerCam.rotation.x, playerCam.rotation.y, playerCam.rotation.z);
        StartCoroutine(CinemaCam());
    }

    private IEnumerator CinemaCam()
    {
        cam.isEvent = true;
        controller.SendMessage("SetPlay", false, SendMessageOptions.DontRequireReceiver);
        player.enabled = false;
        cine.enabled = true;
        //cinema[0].transform.position = playerCam.position;
        //cinema[0].transform.rotation = Quaternion.Euler(playerCam.rotation.x, playerCam.rotation.y, playerCam.rotation.z);
        yield return new WaitForSeconds(1.5f);
        Debug.Log("시작함");
        cinema[1].transform.position = playerCam.position;
        yield return new WaitForSeconds(1.5f);
        print("탱크 줌인");
        cinema[1].Priority = 10;
        cinema[0].Priority = 4;
        yield return new WaitForSeconds(1.0f);
        print("포 쏠 준비");
        cinema[1].enabled = false;
        yield return new WaitForSeconds(3.0f);
        print("포 쏨");
        cinema[2].enabled = false;
        yield return new WaitForSeconds(1.5f);
        print("탱크 밑");
        // 여기까지 총구 돌림
        cinema[3].enabled = false;
        yield return new WaitForSeconds(5.0f);
        cinema[4].enabled = false;
        yield return new WaitForSeconds(4.0f); // 이동하는데 5초
        cinema[0].Priority = 10;
        yield return new WaitForSeconds(1.0f);
        controller.SendMessage("SetPlay", true, SendMessageOptions.DontRequireReceiver);

        // 다시 게임 실행하게 하는 스크립트
        player.enabled = true;
        cine.enabled = false;
        cam.isEvent = false;
        spawner.SendMessage("SpawnerStarter", SendMessageOptions.DontRequireReceiver);
        controller.ZombieAttackDisUp();
    }
}