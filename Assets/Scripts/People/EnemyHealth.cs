﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyHealth : MonoBehaviour
{
    public ObejctKind KIND;

    public int maxHP;
    public bool isDead;
    public GameObject zombie;
    public GameObject explosion;
    public ParticleSystem hitEffect;

    private int _HP;
    private Animator animator;
    private new SkinnedMeshRenderer renderer;
    private PoolManager PM;
    private AudioManager AM;

    // 장애물 데미지 시간
    private float obstacleTime = 0.5f;
    private float obstacleTimer;

    void Awake()
    {
        animator = GetComponent<Animator>();
        renderer = transform.GetChild(transform.childCount - 1).GetComponent<SkinnedMeshRenderer>();
        PM = FindObjectOfType<PoolManager>();
        AM = FindObjectOfType<AudioManager>();
    }

    public int HP
    {
        get { return _HP; }
        set
        {
            _HP = value;

            // 피격모션?? 넣을까?? 말까?? 일단 색만 변경 DOTween으로 색 점차 빨간색에서 원래 색으로 돌아오게 하기
            renderer.material.DOBlendableColor(Color.red, 1.0f).From();
            hitEffect.Play();

            if (HP <= 0)
                Die();
        }
    }
    private void OnEnable()
    {
        _HP = maxHP;
    }

    private void Update()
    {
        obstacleTimer += Time.deltaTime;
    }

    // asdasdaasdasda
    // 대충 죽는 함수라는 뜻
    private void Die()
    {
        isDead = true;
        foreach (Collider c in GetComponents<Collider>())
            c.enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        Vector3 pos = transform.position;
        Quaternion rot = transform.rotation;
        animator.SetTrigger("IsDead");
        StartCoroutine(AfterDie(pos, rot));
    }

    private IEnumerator AfterDie(Vector3 pos, Quaternion rot)
    {
        yield return new WaitForSeconds(1.5f);
        Instantiate(explosion, pos, rot);
        GameObject zom = PM.GetPooledObject(ObejctKind.ZOMBIE, transform.position);
        //zom.transform.position = pos;
        zom.transform.rotation = rot;
        zom.transform.SetParent(GameObject.FindGameObjectWithTag("ZombieParent").transform);

        PM.SetPooledObject(gameObject, KIND);
    }

    // 종류별로 다시 풀링 해줌
    private void Pooling()
    {
        switch (KIND)
        {
            case ObejctKind.CITIZEN_M:
                break;
            case ObejctKind.CITIZEN_W:
                break;
            case ObejctKind.POLICE_BAT:
                break;
            case ObejctKind.POLICE_PISTOL:
                break;
        }
    }

    private void GetDamage(int damage)
    {
        if (!isDead)
            HP -= damage;
    }

    public void GetDamageFromObstacle(int damage)
    {
        if (!isDead)
            if (obstacleTime <= obstacleTimer)
            {
                obstacleTimer = 0;
                HP -= damage;
            }
    }

    public int GetHP()
    {
        return _HP;
    }
}
