﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceAI : MonoBehaviour
{
    public enum State
    {
        MOVEMENT, ATTACK, GOPREVPOS
    }
    public State STATE;

    private PoliceMovement movement;
    private PoliceAttack attack;
    private EnemyHealth health;

    private void Awake()
    {
        movement = GetComponent<PoliceMovement>();
        attack = GetComponent<PoliceAttack>();
        health = GetComponent<EnemyHealth>();
    }

    private void OnEnable()
    {
        StartCoroutine(StateCheck());
    }

    private bool isZombie;

    private void Update()
    {
        if (health.isDead)
            return;
        switch(STATE)
        {
            case State.MOVEMENT:
                movement.Movement = true;
                break;
            case State.ATTACK:
                attack.Attack = true;
                break;
            case State.GOPREVPOS:
                movement.IsPrevPos = true;
                break;
        }
    }

    private IEnumerator StateCheck()
    {
        while(true)
        {
            // 상태 조건 검사
            if (STATE == State.ATTACK && attack.IsTrace())
            {
                STATE = State.GOPREVPOS;
                isZombie = false;
            }

            else if (isZombie)
            {
                STATE = State.ATTACK;
            }

            else if (attack == null)
            {
                STATE = State.GOPREVPOS;
            }

            else if (STATE == State.GOPREVPOS && movement.PrevPosArrive())
                STATE = State.MOVEMENT;

            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (isZombie)
            return;

        if(col.tag == "Zombie")
        {
            isZombie = true;
        }
    }
}
