﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PoliceMovement : MonoBehaviour
{
    private NavMeshAgent navAgent;
    private Animator animator;

    public float speed;
    public float dist;
    public Transform target;

    // 애니메이션에 따라 다르게 설정 하는데 일단 속도 따로 하기
    // 걸으면서 정찰하는게 더 자연스러움
    public float runSpeed;
    public float walkSpeed;

    // 패트롤 포지션
    public List<Transform> patrolPos = new List<Transform>();
    private Transform currentPatrolPos;
    private int patrolPosIndex = 0;

    // 적을 따라가는 데 걸리는 시간
    public float traceTime;
    private float _traceTime = 0;

    // 적을 따라가는데 멈출 거리 (적을 거리로 둘까 아니면 원래 있던 곳에서 거리로 둘까?? -> 원래 있던 거리에서 재는게 좋을거 같음)
    public float traceDist;


    // 적을 찾아 이동하다가 놓치면 prevPos로 올 수 있게 함 -> 원래 있던 자리임
    private Vector3 prevPos;

    private bool move;

    // 처음 상태가 패트롤인지 아니면 가만히 멍때리는건지 확인하는 변수
    public enum State
    {
        PATROL, IDLE
    }
    public State FIRSTSTATE;

    // 원래 주어진 움직임을 수행하는 프로퍼티 
    private bool _movement;
    public bool Movement
    {
        get { return _movement; }
        set
        {
            _movement = value;

            if(Movement)
            {
                // 대충 움직이는 함수
                SetMove();
            }
        }
    }
    
    // 원래 있던 자리로 돌아가는 프로퍼티
    private bool _isGoPrevPos;
    public bool IsPrevPos
    {
        get { return _isGoPrevPos; }
        set
        {
            _isGoPrevPos = value;

            if(_isGoPrevPos)
            {
                // 대충 집에 가는 함수
                GoPrevPos();
            }
        }
    }

    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        move = true;
        switch(FIRSTSTATE)
        {
            case State.IDLE:
                prevPos = transform.position;
                break;
            case State.PATROL:
                prevPos = patrolPos[0].position;
                break;
        }
    }

    // AI스크립트에서 이걸 실행 시키는걸로
    private void SetMove()
    {
        switch(FIRSTSTATE)
        {
            case State.IDLE:
                IdleMotionStarter();
                break;
            case State.PATROL:
                DoPatrol();
                break;
        }
    }

    // 대충 원래 있던 자리로 가는 함수
    private void GoPrevPos()
    {
        move = true;
        navAgent.isStopped = false;
        navAgent.SetDestination(prevPos);

        // 애니메이션을 run?? walk??
        // 속도 줄이기
        animator.SetBool("IsWalking", true);
        navAgent.speed = walkSpeed;
    }

    // 대충 패트롤 하는 함수
    private void DoPatrol()
    {
        // 최적의 경로가 아니면 리턴
        move = true;
        if (navAgent.isPathStale)
            return;

        if (navAgent.remainingDistance <= 0.1f)
        {
            if (patrolPos.Count == 1)
            {
                FIRSTSTATE = State.IDLE;
            }
            else
                SetPatrolPos();
        }

        navAgent.SetDestination(patrolPos[patrolPosIndex].position);
        navAgent.isStopped = false;

        animator.SetBool("IsWalking", true);
        navAgent.speed = walkSpeed;
    }

    // 패트롤 포지션을 다시 재조정 해주는 함수
    private void SetPatrolPos()
    {
        move = true;
        //currentPatrolPos = patrolPos[patrolPosIndex];
        patrolPosIndex++;
        patrolPosIndex %= patrolPos.Count;
        prevPos = patrolPos[patrolPosIndex].position;
    }

    // Idle 상태일때 가만히 있기는 뭐하니까 일정 시간에 한번씩 Y축만 뱅글뱅글 돌려주는 함수
    private void IdleMotionStarter()
    {
        if(move)
        {
            animator.SetBool("IsIdle", true);
            animator.SetBool("IsRunning", false);
            animator.SetBool("IsWalking", false);
            move = false;
            StartCoroutine(IdleMotion());
        }
    }

    private IEnumerator IdleMotion()
    {
        Quaternion originRot = transform.rotation;
        while(!move)
        {
            float randomTime = Random.Range(5.0f, 15.0f);
            navAgent.isStopped = true;
            transform.DOLocalRotate(new Vector3(originRot.x, Random.Range(0, 360f), originRot.z), 0.5f);
            yield return new WaitForSeconds(randomTime);
        }
        yield break;
    }
    // 여기까지 idle

    public bool PrevPosArrive()
    {
        Debug.Log(navAgent.remainingDistance);
        if (navAgent.remainingDistance < 0.5f)
            return true;
        return false;
    }

    private void SetFirstState(ArrayList info)
    {
        FIRSTSTATE = (State)((int)info[0]);
        patrolPos.Clear();
        patrolPos.Add((Transform)info[1]);
    }
}
