﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PoliceAttack : MonoBehaviour
{
    public enum Weapon
    {
        BAT, PISTOL, RIFLE
    }
    public Weapon WEAPON;

    public float damage;

    // 적을 발견 했을때 가는곳은 player로 -> 가운데 있으니까
    private Transform player;

    // 공격 타겟
    public Transform target;
    // 공격 사정거리
    public float attackDist;
    // 공격 쿨타임
    public float attackTime;
    private float _attackTime;

    // 적을 따라가는 데 걸리는 시간
    public float traceTime;
    private float _traceTime = 0;

    // 적을 따라가는데 멈출 거리 (적을 거리로 둘까 아니면 원래 있던 곳에서 거리로 둘까?? -> 원래 있던 거리에서 재는게 좋을거 같음)
    public float traceDist;

    // 타겟이 있는지 없는지 확인하는 bool 함수
    private bool haveTarget;

    // 좀비 무리
    private Transform zombies;
    
    // 쓸 컴포넌트들
    private NavMeshAgent navAgent;
    private Animator animator;
    private AudioManager AM;

    // 꿘총 및 소총에만 있는 애들만 쓸 컴포넌트들
    [Header("HavePostol")]
    public Transform muzzle;
    private Light gunLight;
    private LineRenderer gunLine;
    public float gunRange;
    // 소총 사격 횟수
    public int rifleShootCount;


    private bool _attack;

    public bool Attack
    {
        get { return _attack; }

        set
        {
            _attack = value;
            if(_attack)
            {
                // 공격하는 함수
                ToTarget();
            }
        }
    }

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        zombies = GameObject.FindGameObjectWithTag("ZombieParent").transform;
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        AM = FindObjectOfType<AudioManager>();
        if(WEAPON == Weapon.PISTOL || WEAPON == Weapon.RIFLE)
        {
            gunLight = muzzle.GetComponent<Light>();
            gunLine = muzzle.GetComponent<LineRenderer>();
        }
    }

    void OnEnable()
    {
        haveTarget = false;
        if (WEAPON == Weapon.BAT)
            attackDist = 3;
        else
            DisableEffect();
    }

    void Update()
    {
        _attackTime += Time.deltaTime;
    }

    private void ToTarget()
    {
        SetTarget();
        if(target.GetComponent<ZombieHealth>().isDead)
            SetTarget();
        _traceTime += Time.deltaTime;
        navAgent.SetDestination(target.position);
        animator.SetBool("IsRunning", true);
        // 대충 시간과 거리에 따라 그만 한다는 소스 근디 이건 AI에 넣어야 할듯
        // 애니메이션을 run으로 하고 속도 UP
        if (Vector3.Distance(transform.position, target.position) <= attackDist)
        {
            animator.SetBool("IsRunning", false);
            navAgent.isStopped = true;
            switch (WEAPON)
            {
                case Weapon.BAT:
                    BatAttack();
                    break;
                case Weapon.PISTOL:
                    PistolAttack();
                    break;
                case Weapon.RIFLE:
                    RifleAttack();
                    break;
            }
        }
        else navAgent.isStopped = false;

        transform.LookAt(target);
    }

    // 배트 공격은 사거리 1로 고정했음 근디 이건 바꿔보고 괜찮은걸로 하기
    private void BatAttack()
    {
        // ToTarget돌다가 사정거리가 타겟에 닿게?? 아니면 oncoltrigger 를 써서 할까??? 
        // oncol로 하는게 좋을듯
        // 저 함수 돌다가 좀비 나오면 타겟으로 설정 한 뒤 공격
        // 아니면 좀비 무리들 중 가장 가까운 좀비를 타겟으로? 그건 5초에 한번씩 발동하는걸로
        if(attackTime <= _attackTime)
        {
            AM.PlaySound("HitClub01", transform.position);
            animator.SetTrigger("IsAttack");
            _attackTime = 0;
            navAgent.isStopped = true;
            target.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);

            // 공격모션 넣기
            // 소리도 넣기
            //animator.SetTrigger("IsAttack");
            animator.SetBool("IsRunning", false);
        }
    }

    private void PistolAttack()
    {
        if(attackTime <= _attackTime)
        {
            AM.PlaySound("pistol_sound", transform.position);
            animator.SetTrigger("IsGunAttack");
            _attackTime = 0;
            navAgent.isStopped = true;
            //총구 방향으로 나가는 Ray를 생성
            Ray shootRay = new Ray(muzzle.transform.position, transform.forward);
            //그 Ray를 Raycast해서, 맞으면 RaycastHit값을 얻어옴
            if (Physics.Raycast(shootRay, out RaycastHit hit, gunRange))
            {
                //RaycastHit을 통해 EnemyHealth 컴포넌트를 가져옴
                if(hit.collider.tag == "Zombie")
                {
                    hit.transform.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
                }
            }
            gunLight.enabled = true;
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * gunRange);
            CancelInvoke("DisableEffect");
            Invoke("DisableEffect", 0.03f);
        }
    }

    private void RifleAttack()
    {
        if (attackTime <= _attackTime)
        {
            StartCoroutine(RifleEffect());
        }
    }

    private IEnumerator RifleEffect()
    {
        animator.SetTrigger("IsGunAttack");
        for (int i = 0; i < rifleShootCount; i++)
        {
            AM.PlaySound("rifle_sound", transform.position);
            _attackTime = 0;
            navAgent.isStopped = true;
            //총구 방향으로 나가는 Ray를 생성
            Ray shootRay = new Ray(muzzle.transform.position, transform.forward);
            //그 Ray를 Raycast해서, 맞으면 RaycastHit값을 얻어옴
            if (Physics.Raycast(shootRay, out RaycastHit hit, gunRange))
            {
                //RaycastHit을 통해 EnemyHealth 컴포넌트를 가져옴
                if (hit.collider.tag == "Zombie")
                {
                    hit.transform.SendMessage("GetDamage", damage, SendMessageOptions.DontRequireReceiver);
                }
            }
            gunLight.enabled = true;
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * gunRange);
            CancelInvoke("DisableEffect");
            Invoke("DisableEffect", 0.03f);
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }

    void DisableEffect()
    {
        gunLight.enabled = false;
        gunLine.enabled = false;
    }

    private IEnumerator PistolMotion()
    {
        yield break;
    }

    // 좀비 무리중에 가장 가까운 좀비의 Transform값 받아오는 함수
    private void SetTarget()
    {
        if (!haveTarget || zombies.childCount > 0)
        {
            float dist = Mathf.Infinity;
            int shortIndex = 0;
            for (int i = 0; i < zombies.childCount; i++)
            {
                float _dist;
                _dist = Vector3.Distance(transform.position, zombies.GetChild(i).transform.position);
                if (dist > _dist)
                {
                    dist = _dist;
                    shortIndex = i;
                }
            }
            haveTarget = true;
            target = zombies.GetChild(shortIndex);
        }
        else
            GetComponent<PoliceAI>().STATE = PoliceAI.State.GOPREVPOS;
    }

    // 일정 거리 밖이거나 아니면 일정 시간동안 타겟에게 가면 false 를 반환해서 AI 스크립트에서 알 수 있게 하기
    public bool IsTrace()
    {
        // 일정거리 밖에 있음 및 일정시간동안 이동했음
        if (Vector3.Distance(transform.position, target.position) < traceDist || traceTime <= _traceTime)
        {
            _traceTime = 0;
            haveTarget = false;
            return false;
        }
        else return true;
    }
}
