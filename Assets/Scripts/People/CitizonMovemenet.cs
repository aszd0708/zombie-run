﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CitizonMovemenet : MonoBehaviour
{
    private bool find = false;
    private NavMeshAgent navAgent;
    private Animator animator;
    private Transform target;
    private EnemyHealth health;
    private bool idle = true;
    void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        health = GetComponent<EnemyHealth>();
    }

    void Update()
    {
        if (!find)
        {
            animator.SetBool("IsRunning", false);
            if(idle)
                StartCoroutine(IdleMotion());   
            return;
        }
        // 튀면 모션 변경
        animator.SetBool("IsRunning", true);
        navAgent.isStopped = false;
        navAgent.SetDestination((transform.position-target.transform.position).normalized * 10);
        float targetDist = Vector3.Distance(target.position, transform.position);

        if (targetDist >= 10f)
        {
            find = false;
            navAgent.isStopped = true;
        }
        if (health.isDead)
            this.enabled = false;
    }

    void OnTriggerStay(Collider col)
    {
        if (find)
            return;

        if (col.tag == "Zombie" || col.CompareTag("Player"))
        {
            target = col.transform;
            find = true;
        }
    }

    private IEnumerator IdleMotion()
    {
        idle = false;
        Quaternion originRot = transform.rotation;
        while (!find)
        {
            float randomTime = Random.Range(5.0f, 15.0f);
            navAgent.isStopped = true;
            transform.DOLocalRotate(new Vector3(originRot.x, Random.Range(0, 360f), originRot.z), 0.5f);
            yield return new WaitForSeconds(randomTime);
        }
        yield break;
    }

    private IEnumerator RunAway(Transform target)
    {
        navAgent.isStopped = false;
        while (!find)
        {
            SetDestination(target);
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }

    private void SetDestination(Transform target)
    {
        Debug.Log(-target.transform.forward);
        navAgent.SetDestination(-target.transform.forward);
        float targetDist = Vector3.Distance(target.position, transform.position);

        if (targetDist >= 10f)
        {
            Debug.Log("타겟과 멀어짐");
            find = false;
            navAgent.isStopped = true;
        }
    }
}
